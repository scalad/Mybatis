##MyBatis 文件
####什么是 Mybatis?<br>
&nbsp;&nbsp;&nbsp;&nbsp;MyBatis 是支持普通 SQL 查询，存储过程和高级映射的优秀持久层框架。MyBatis 消除了几乎所有的 JDBC 代码和参数的手工设置以及结果集的检索。MyBatis 使用简单的 XML 或注解用于配置和原始映射，将接口和 Java的 POJOs（Plan Old Java Objects，普通的 Java 对象）映射成数据库中的记录。<br>
####orm工具的基本思想
无论是用过的 hibernate,Mybatis,你都可以法相他们有一个共同点：<br>
1. 从配置文件(通常是 XML 配置文件中)得到 sessionfactory.<br>
2. 由 sessionfactory 产生 session<br>
3. 在 session 中完成对数据的增删改查和事务提交等.<br>
4. 在用完之后关闭 session 。<br>
5. 在 Java 对象和 数据库之间有做 mapping 的配置文件,也通常是xml文件<br>
####创建mybatis测试程序
这里使用maven搭建，因为后期的关系，maven中集成了Spring库文件<br>
#
	<dependency>
		<groupId>org.mybatis</groupId>
		<artifactId>mybatis</artifactId>
		<version>x.x.x</version>
	</dependency>
#
(1)项目中放置了创建数据库的脚本sql.sql<br>
(2)src/main/resources放置了log4j.properties日期配置文件MyBatisConfig.xml为mybatis的配置文件<br>
(3)com.silence.mybatis.enties下的User、Article为实体类，User.xml为mybatis映射配置文件
(4)com.silence.mybatis.inter下的IUser为User类的接口设计<br>
(5)com.silence.mybatis下UserTest为该程序的测试文件，实现的对User的增删改查以及关联查询等操作
####MyBatisConfig.xml核心配置文件
(1)该配置文件是mybatis用来建立sessionFactory 用的，里面主要包含了数据库连接相关东西，还有 java 类所对应的别名，比如 <typeAlias alias="User" type="com.silence.ssm.entity.User"/> 这个别名非常重要，你在 具体的类的映射中，比如 User.xml 中 resultType 就是对应这里的。要保持一致，当然这里的 resultType 还有另外单独的定义方式，后面再说。<br>

#
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE configuration
	    PUBLIC "-//mybatis.org//DTD Config 3.0//EN""http://mybatis.org/dtd/mybatis-3-config.dtd">
	<configuration>
		<!-- 为User起一个别名,可以在 resultType中使用 -->
		<typeAliases>
			<typeAlias alias="User" type="com.silence.mybatis.enties.User" />
			<typeAlias alias="Article" type="com.silence.mybatis.enties.Article" />
			<typeAlias alias="Teacher" type="com.silence.mybatis.enties.Teacher" />
			<typeAlias alias="Classes" type="com.silence.mybatis.enties.Classes" />
			<typeAlias alias="Student" type="com.silence.mybatis.enties.Student" />
		</typeAliases>
		<environments default="development">
			<environment id="development">
				<transactionManager type="JDBC">
				</transactionManager>
				<dataSource type="POOLED">
					<property name="driver" value="com.mysql.jdbc.Driver" />
					<property name="url" value="jdbc:mysql://localhost:3306/mybatis" />
					<property name="username" value="root" />
					<property name="password" value="root" />
				</dataSource>
			</environment>
		</environments>
		<mappers>
			<mapper resource="com/silence/mybatis/mapper/User.xml" />
			<mapper resource="com/silence/mybatis/mapper/CategoryMapper.xml" />
			<mapper resource="com/silence/mybatis/mapper/OrderMapper.xml" />
			<mapper resource="com/silence/mybatis/mapper/ClassMapper.xml" />
			<mapper resource="com/silence/mybatis/mapper/CustomerMapper.xml" />
			<mapper resource="com/silence/mybatis/mapper/PUserMapper.xml" />
		</mappers>
	</configuration>
#

(2)里面的<mapper resource="com/silence/ssm/entity/User.xml"/> 是包含要映射的类的 xml 配置文件。<br>
(3)在 User.xml 文件里面 主要是定义各种 SQL 语句，以及这些语句的参数，以及要返回的类型等。<br>

#
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
	<!-- 用接口的方式编程,。这种方式，要注意的一个地方就是。在 User.xml 的配置文件中， <mapper namespace="com.silence.mybatis.enties.IUser">命名空间 
	非常重要，不能有错，必须与我们定义的package 和 接口一致。如果不一致就会出错 -->
	<mapper namespace="com.silence.mybatis.inter.IUser">
	<!-- resultType="User"使用别名 -->
	<select id="findById" parameterType="int" resultType="User">
		select * from user where id=#{id}
	</select>

	<!-- 为了返回list 类型而定义的returnMap -->
	<resultMap type="User" id="resultListUser">
		<id column="id" property="id" />
		<result column="userName" property="userName" />
		<result column="passWord" property="passWord" />
	</resultMap>
	<!-- 返回list 的select 语句，注意 resultMap 的值是指向前面定义好的 -->
	<select id="selectUsers" parameterType="string" resultMap="resultListUser">
		select * from user where userName like #{userName}
	</select>

	<!--执行增加操作的SQL语句。id和parameterType 分别与IUser接口中的addUser方法的名字和 参数类型一致。 以#{name}的形式引用User参数 
		的name属性，MyBatis将使用反射读取User参数 的此属性。#{name} 中name大小写敏感。引用其他 的gender等属性与此一致。seGeneratedKeys设置 
		为"true"表明要MyBatis 获取由数据库自动生成的主 键；keyProperty="id"指定把获取到的主键值注入 到Student的id属性 -->
	<insert id="addUser" parameterType="User" useGeneratedKeys="true"
		keyProperty="id">
		insert into
		user(username,password)values(#{userName},#{passWord})
	</insert>

	<!-- 更新数据 -->
	<update id="updateUser" parameterType="User">
		update user set username=#{userName},password=#{passWord} where id=#{id}
	</update>
	
	<!-- 删除操作 -->
	<delete id="deleteUser" parameterType="int">
		delete from user where id=#{id}
	</delete>
	
	<!-- User 联合文章进行查询 方法之一的配置 (多对一的方式) -->
	<resultMap id="resultUserArticleList" type="Article">
		<id property="id" column="aid" />
		<result property="title" column="title" />
		<result property="content" column="content" />
		<association property="user" javaType="User">
			<id property="id" column="id" />
			<result property="userName" column="userName" />
			<result property="passWord" column="passWord" />
		</association>
	</resultMap>
	<!-- User 联合文章进行查询 方法之二的配置 (多对一的方式) -->
	<resultMap id="resultUserArticleList-2" type="Article">
		<id property="id" column="aid" />
		<result property="title" column="title" />
		<result property="content" column="content" />
		<association property="user" javaType="User" resultMap="resultListUser" />
	</resultMap>
	<select id="getUserArticles" parameterType="int" resultMap="resultUserArticleList">
		select user.id,user.userName,user.passWord,article.id aid,article.title,article.content from user,article
		where user.id=article.userid and user.id=#{id}
	</select>

	<!-- 分页处理 -->
	<select id="getUserArticlesPage" parameterType="java.util.Map" resultMap="resultUserArticleList">
		select user.id,user.userName,user.passWord,article.id aid,article.title,article.content from user,article
		where user.id=article.userid and user.id=#{id} limit #{offset},#{pagesize}
	</select>
	
	<!-- if测试 -->
	<select id="testIf" parameterType="User" resultMap="resultListUser">
		select * from user where 1 = 1 
		<if test="passWord != null"> and password = #{passWord}</if>
		<if test="userName != null"> and userName = #{userName}</if>
	</select>
	
	<!-- choose测试 ,(when,otherwize) ,相当于 Java 语言中的 switch ,与 jstl 中的choose 很类似-->
	<select id="testChoose" parameterType="User" resultMap="resultListUser">
		select * from user where 1 = 1
		<choose>
			<when test="userName != null">and username = #{userName}</when>
			<when test="passWord != null">and password = #{passWord}</when>
			<otherwise>and username is not null</otherwise>
		</choose>
	</select>
	
	<!-- trim测试 ,对包含的内容加上 prefix,或者 suffix 等，前缀，后缀,trim 元素的主要功能是可以在自己
	包含的内容前加上某些前缀，也可以在其后加上某些后缀，与之对应的属性是prefix 和 suffix；可以把包含内容的首
	部某些内容覆盖，即忽略，也可以把尾部的某些内容覆盖，对应的属性是 prefixOverrides 和 suffixOverrides；
	正因为 trim 有这样的功能，所以我们也可以非常简单的利用 trim 来代替where 元素的功能-->
	<select id="testTrim" parameterType="User" resultMap="resultListUser">
		select * from user
		<trim prefix="where" prefixOverrides="and |or">
			<if test="userName != null">username = #{userName}</if>
			<if test="passWord != null">and passWord = #{passWord}</if>
			<if test="id != null">or id = #{id}</if>
		</trim>
	</select>
	
	<!-- where主要是用来简化 SQL 语句中 where 条件判断的，能智能的处理 and or 条件 
		where 元素的作用是会在写入 where 元素的地方输出一个 where，另外一个好处是你不需要考虑 where 元素
		里面的条件输出是什么样子的，MyBatis 会智能的帮你处理，如果所有的条件都不满足那么 MyBatis 就会查出
  		所有的记录，如果输出后是 and 开头的，MyBatis 会把第一个 and 忽略，当然如果是 or 开头的，MyBatis 
  		也会把它忽略；此外，在 where 元素中你不需要考虑空格的问题，MyBatis 会智能的帮你加上。像上述例子中，如
  		果 title=null， 而 content != null，那么输出的整个语句会是 select * from user where
  		 content = #{content} ，而不是 select * from t_blog where and content = #{content} 
  		 ，因为 MyBatis 会智能的把首个 and 或or 给忽略。-->
	<select id="testWhere" parameterType="User" resultMap="resultListUser">
		select * from user
		<where>
			<if test="userName != null">username = #{userName}</if>
			<if test="passWord != null">and passWord = #{passWord}</if>
			<if test="id != 0">and id = #{id}</if>
		</where>
	</select>
	
	<!-- set主要用于更新时set 元素主要是用在更新操作的时候，它的主要功能和 where 元素其实是差不多的，主要是在包含的语句前输出
		一个set，然后如果包含的语句是以逗号结束的话将会把该逗号忽略，如果 set 包含的内容为空的话则会出错。有了 set 元素我们就可
		以动态的更新那些修改了的字段。-->
	<update id="testUpdate" parameterType="User">
		update user
		<set>
			<if test="userName != null">username = #{userName},</if>
			<if test="passWord != null">password = #{passWord}</if>
		</set>
		where id = #{id}
	</update>
	
	<!-- foreach在实现 mybatis in 语句查询时特别有用foreach 的主要用在构建 in 条件中，它可以在 SQL 
	 语句中进行迭代一个集合。foreach 元素的属性主要有 item，index，collection，open，separator，
	 close。item 表示集合中每一个元素进行迭代时的别名，index指定一个名字，用于表示在迭代过程中，每次迭代到
	 的位置，open 表示该语句以什么开始，separator表示在每次进行迭代之间以什么符号作为分隔符，close 表示以
	 什么结束，在使用 foreach 的时候最关键的也是最容易出错的就是 collection 属性，该属性是必须指定的，但是
	 在不同情况下，该属性的值是不一样的，主要有一下 3 种情况：
		• 如果传入的是单参数且参数类型是一个 List 的时候，collection 属性值为 list
		• 如果传入的是单参数且参数类型是一个 array 数组的时候，collection 的属性值为 array
		• 如果传入的参数是多个的时候，我们就需要把它们封装成一个 Map 了，当然单参数也可以封装成 map，实
		      际上如果你在传入参数的时候，在 MyBatis 里面也是会把它封装成一个 Map 的，map 的 key 就是参数
		      名，所以这个时候 collection 属性值就是传入的 List 或 array 对象在自己封装的 map 里面的 key -->
	<select id="testForeach" resultMap="resultListUser">
		select * from user where id in 
		<foreach collection="list" index="index" item="item" open="(" separator="," close=")">
			#{item}
		</foreach>
	</select>
	<!-- 数组类型的参数 -->
	<select id="testForeachArray" resultMap="resultListUser">
		select * from user where id in 
		<foreach collection="array" index="index" item="item" open="(" separator="," close=")">
			#{item}
		</foreach>
	</select>
	<!-- Map 类型的参数 -->
	<select id="testForeachMap" resultMap="resultListUser">
		select * from user where username like "%"#{userName}"%" and id in 
		<foreach collection="ids" index="index" item="item" open="(" separator="," close=")">
			#{item}
		</foreach>
	</select>
	
	<cache></cache>
	</mapper>
#
##Mybatis自动生成配置文件
Mybatis 应用程序，需要大量的配置文件，对于一个成百上千的数据库表来说，完全手工配置，这是一个很恐怖的工作量. 所以 Mybatis 官方也推出了一个 Mybatis 代码生成工具的 jar 包。
,Mybatis 代码生成工具主要有一下功能:<br>
1. 生成 pojo 与 数据库结构对应<br>
2. 如果有主键，能匹配主键<br>
3. 如果没有主键，可以用其他字段去匹配v
4. 动态 select,update,delete 方法<br>
5. 自动生成接口(也就是以前的 dao 层)<br>
6. 自动生成 sql mapper，增删改查各种语句配置，包括动态 where 语句配置<br>
7. 生成 Example 例子供参考<br>
(1)除了mybatis以及数据库等依赖外你应该还需要加入generator依赖<br>
`<dependency>`<br>
	`<groupId>org.mybatis.generator</groupId>`<br>
	`<artifactId>mybatis-generator-core</artifactId>`<br>
	`<version>1.3.2</version>`<br>
`</dependency>`<br>
(2)在创建的 Web 工程中，创建相应的 package 比如 :<br>
com.silence.mybatis.inter 用来存放 Mybatis接口对象。<br>
com.silence.mybatis.mapper 用来存放 sql mapper 对应的映射，sql语句等。<br>
com.silence.mybatis.enties 用来存放与数据库对应的model实体对象 。<br>
在用 Mybatis 代码生成工具之前，这些目录必须先创建好，作为一个好的应用程序，这些目录的创建也是有规律的。根据 Mybatis 代码生成工具文档，需要一个配置文件，这里命名为:mbgConfiguration.xml 放在maven的resouces目录下. 配置文件内容如下:<br>
# 	
	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE generatorConfiguration
	PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
	"http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
	<generatorConfiguration>
	<!-- 配置mysql 驱动jar包路径.用了绝对路径 -->
	<classPathEntry
	location="D:\mavenRepository\mysql\mysql-connector-java\5.1.30\mysql-connector-java-5.1.30.jar" />
	<context id="yihaomen_mysql_tables" targetRuntime="MyBatis3">
	<!-- 为了防止生成的代码中有很多注释，比较难看，加入下面的配置控制 -->
	<commentGenerator>
		<property name="suppressAllComments" value="true" />
		<property name="suppressDate" value="true" />
	</commentGenerator>
	<!-- 注释控制完毕 -->
	<!-- 数据库连接 -->
	<jdbcConnection driverClass="com.mysql.jdbc.Driver"
		connectionURL="jdbc:mysql://127.0.0.1:3306/mybatis?characterEncoding=utf8"
		userId="root" password="root">
	</jdbcConnection>
	<javaTypeResolver>
		<property name="forceBigDecimals" value="false" />
	</javaTypeResolver>
	<!-- 数据表对应的model 层 -->
	<javaModelGenerator targetPackage="com.silence.mybatis.enties"
		targetProject="src/main/java">
		<property name="enableSubPackages" value="true" />
		<property name="trimStrings" value="true" />
	</javaModelGenerator>
	<!-- sql mapper 隐射配置文件 -->
	<sqlMapGenerator targetPackage="com.silence.mybatis.mapper"
		targetProject="src/main/java">
		<property name="enableSubPackages" value="true" />
	</sqlMapGenerator>
	<!-- 在ibatis2 中是dao层，但在mybatis3中，其实就是mapper接口 -->
	<javaClientGenerator type="XMLMAPPER"
		targetPackage="com.silence.mybatis.inter" targetProject="src/main/java">
		<property name="enableSubPackages" value="true" />
	</javaClientGenerator>
	<!-- 要对那些数据表进行生成操作，必须要有一个. -->
	<table schema="mybatis" tableName="category" domainObjectName="Category"
		enableCountByExample="false" enableUpdateByExample="false"
		enableDeleteByExample="false" enableSelectByExample="false"
		selectByExampleQueryId="false">
	</table>
	</context>
	</generatorConfiguration>
#
(3)接下来我们创建测试文件来生成该配置文件
#
	package com.silence.mybatis;
	
	import java.io.File;
	import java.io.IOException;
	import java.sql.SQLException;
	import java.util.ArrayList;
	import java.util.List;
	
	import org.junit.Test;
	import org.mybatis.generator.api.MyBatisGenerator;
	import org.mybatis.generator.config.Configuration;
	import org.mybatis.generator.config.xml.ConfigurationParser;
	import org.mybatis.generator.exception.InvalidConfigurationException;
	import org.mybatis.generator.exception.XMLParserException;
	import org.mybatis.generator.internal.DefaultShellCallback;
	
	public class GenMain {
	@Test
	public void test() {
		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		String genCfg = "/mbgConfiguration.xml";
		File configFile = new File(GenMain.class.getResource(genCfg).getFile());
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = null;
		try {
			config = cp.parseConfiguration(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XMLParserException e) {
			e.printStackTrace();
		}
		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGenerator = null;
		try {
			myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		try {
			myBatisGenerator.generate(null);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	}
#
(4)执行上面这一段junit测试程序，然后刷新当前工程，我们可以看见，在我们的工程下生成了<br>
实体类
#	
	package com.silence.mybatis.enties;
	public class Article {
	private int id;
	private User user;
	private String title;
	private String content;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	}
#
实体类接口
#
	package com.silence.mybatis.inter;
	import com.silence.mybatis.enties.Category;
	public interface CategoryMapper {
    	int deleteByPrimaryKey(Integer id);
    	int insert(Category record);
    	int insertSelective(Category record);
    	Category selectByPrimaryKey(Integer id);
    	int updateByPrimaryKeySelective(Category record);
    	int updateByPrimaryKey(Category record);
	}
#
映射文件
#
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
	<mapper namespace="com.silence.mybatis.inter.CategoryMapper" >
	  <resultMap id="BaseResultMap" type="com.silence.mybatis.enties.Category" >
	    <id column="id" property="id" jdbcType="INTEGER" />
	    <result column="catname" property="catname" jdbcType="VARCHAR" />
	    <result column="catdescription" property="catdescription" jdbcType="VARCHAR" />
	  </resultMap>
	  <sql id="Base_Column_List" >
	    id, catname, catdescription
	  </sql>
	  <select id="selectByPrimaryKey" resultMap="BaseResultMap" parameterType="java.lang.Integer" >
	    select 
	    <include refid="Base_Column_List" />
	    from category
	    where id = #{id,jdbcType=INTEGER}
	  </select>
	  <delete id="deleteByPrimaryKey" parameterType="java.lang.Integer" >
	    delete from category
	    where id = #{id,jdbcType=INTEGER}
	  </delete>
	  <insert id="insert" parameterType="com.silence.mybatis.enties.Category" >
	    insert into category (id, catname, catdescription
	      )
	    values (#{id,jdbcType=INTEGER}, #{catname,jdbcType=VARCHAR}, #{catdescription,jdbcType=VARCHAR}
	      )
	  </insert>
	  <insert id="insertSelective" parameterType="com.silence.mybatis.enties.Category" >
	    insert into category
	    <trim prefix="(" suffix=")" suffixOverrides="," >
	      <if test="id != null" >
	        id,
	      </if>
	      <if test="catname != null" >
	        catname,
	      </if>
	      <if test="catdescription != null" >
	        catdescription,
	      </if>
	    </trim>
	    <trim prefix="values (" suffix=")" suffixOverrides="," >
	      <if test="id != null" >
	        #{id,jdbcType=INTEGER},
	      </if>
	      <if test="catname != null" >
	        #{catname,jdbcType=VARCHAR},
	      </if>
	      <if test="catdescription != null" >
	        #{catdescription,jdbcType=VARCHAR},
	      </if>
	    </trim>
	  </insert>
	  <update id="updateByPrimaryKeySelective" parameterType="com.silence.mybatis.enties.Category" >
	    update category
	    <set >
	      <if test="catname != null" >
	        catname = #{catname,jdbcType=VARCHAR},
	      </if>
	      <if test="catdescription != null" >
	        catdescription = #{catdescription,jdbcType=VARCHAR},
	      </if>
	    </set>
	    where id = #{id,jdbcType=INTEGER}
	  </update>
	  <update id="updateByPrimaryKey" parameterType="com.silence.mybatis.enties.Category" >
	    update category
	    set catname = #{catname,jdbcType=VARCHAR},
	      catdescription = #{catdescription,jdbcType=VARCHAR}
	    where id = #{id,jdbcType=INTEGER}
	  </update>
	</mapper>
#
(5)写测试类，当然，首先需要将CategoryMapper.xml加入到mybatis核心配置下<br>
#
	<mapper resource="com/silence/mybatis/mapper/CategoryMapper.xml" />
#
#
	package com.silence.mybatis;
	import java.io.IOException;
	import java.io.Reader;
	import org.apache.ibatis.io.Resources;
	import org.apache.ibatis.session.SqlSession;
	import org.apache.ibatis.session.SqlSessionFactory;
	import org.apache.ibatis.session.SqlSessionFactoryBuilder;
	import org.junit.After;
	import org.junit.Before;
	import org.junit.Test;
	
	import com.silence.mybatis.enties.Category;
	import com.silence.mybatis.inter.CategoryMapper;
	
	public class CategoryTest {
		private SqlSessionFactory sqlMapper = null;
		private SqlSession session = null;
	
		@Test
		public void testdeleteByPrimaryKey(){
			CategoryMapper mapper = session.getMapper(CategoryMapper.class);
			Category category = mapper.selectByPrimaryKey(1);
			System.out.println(category);
		}
		@Before
		public void init() {
			String resource = "MyBatisConfig.xml";
			Reader reader = null;
			try {
				reader = Resources.getResourceAsReader(resource);
			} catch (IOException e) {
				e.printStackTrace();
			}
			sqlMapper = new SqlSessionFactoryBuilder().build(reader);
			session = sqlMapper.openSession();
			session.commit(false);
		}
	
		@After
		public void destory() {
			session.commit();
			session.close();
		}
	}	
#
###注意事项
如果你想生成 example 之类的东西，需要在`<table></table>` 里面去掉
#	
	enableCountByExample="false" enableUpdateByExample="false"
	enableDeleteByExample="false" enableSelectByExample="false"
	selectByExampleQueryId="false"
#
这部分配置，这是生成 example 而用的，一般来说对项目没有用。另外生成的 sql mapper 等，只是对单表的增删改查，如果你有多表 join 操作，你就可以手动配置，如果调用存储过程，你也需要手工配置. 这时工作量已经少很多了。如果你想用命令行方式处理，也是可以的。比如：<br>
	java -jar mybatis-generator-core-1.3.2.jar -mbgConfiguration.xm -overwrite<br>
这时，要用绝对路径才行. 另外 mbgConfiguration.xml 配置文件中 targetProject 的配置也必须是绝对路径了。

###在控制台显示 SQL 语句
用过 Hibernate 的人都知道，Hibernate 是可以配置 show_sql 显示 自动生成的 SQL 语句，用 format_sql 可以格式化 SQL 语句，但如果用 Mybatis 怎么实现这个功能呢, 可以通过配置日志来实现的，比如配置我们最常用的 log4j.properties 来实现。<br>
#
	log4j.rootCategory=info, stdout , R
	log4j.appender.stdout=org.apache.log4j.ConsoleAppender
	log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
	log4j.appender.stdout.layout.ConversionPattern=[QC] %p [%t] %C.%M(%L) | %m%n
	log4j.appender.R=org.apache.log4j.DailyRollingFileAppender
	log4j.appender.R.File=D:/my_log.log
	log4j.appender.R.layout=org.apache.log4j.PatternLayout
	log4j.appender.R.layout.ConversionPattern=%d-[TS] %p %t %c - %m%n
	log4j.logger.com.ibatis=debug
	log4j.logger.com.ibatis.common.jdbc.SimpleDataSource=debug
	log4j.logger.com.ibatis.common.jdbc.ScriptRunner=debug
	log4j.logger.com.ibatis.sqlmap.engine.impl.SqlMapClientDelegate=debug
	log4j.logger.java.sql.Connection=debug
	log4j.logger.java.sql.Statement=debug
	log4j.logger.java.sql.PreparedStatement=debug,stdout
#
##传递多个参数的方法
在用 Mybatis 做查询的时候，通常会传递多个参数，一般来说，这种情况下有两种解决办法：<br>
• 利用 hashMap 去做。<br>
• 利用 Mybatis 自身的多个参数传递方式去做<br>
利用 hashMap 传递多个参数<br>
#
	<select id="selectByDate" parameterType="map" resultMap="campaignStats">
		Select * FROM CampaignStats Where statsDate >= #{start} AND 
		statsDate <= #{end}
	</select>
	public List<DpCampaignStats> selectByDate(Date start, Date end){
		SqlSession session = sqlSessionFactory.openSession();
		try {
			Map<String, Date> map = new HashMap<String, Date>();
			map.put("start", start);
			map.put("end", end);
			List<DpCampaignStats> list = session.
			selectList("DpCampaignStats.selectByDate", map);
			return list;
		} finally {
		session.close();
		}
	}
#
Mybatis 自带的多个参数传递方法
#
	<select id="selectByDate" resultMap="campaignStats">
		Select * FROM CampaignStats Where statsDate >= #{param1} 
		AND statsDate <= #{param2}
	</select>
	请注意，这个时候没有 parameterType, 但用到了类似 #{param1} 类似的参数. 
	同样 Java 代码也需要做出改变
	public List<DpCampaignStats> selectByDate(Date start, Date end){
		SqlSession session = sqlSessionFactory.openSession();
		try {
		List<DpCampaignStats> list = session.
			selectList("DpCampaignStats.selectByDate", start,end);
			return list;
		} finally {
		session.close();
		}
	}
	推荐使用 hashMap 来传递多个参数。
#

## 一对一关联
创建表的基本信息

	根据班级 id 查询班级信息(带老师的信息)<br>
	CREATE TABLE teacher(
	t_id INT PRIMARY KEY AUTO_INCREMENT,
	t_name VARCHAR(20)
	);
	CREATE TABLE class(
	c_id INT PRIMARY KEY AUTO_INCREMENT,
	c_name VARCHAR(20),
	teacher_id INT
	);
	ALTER TABLE class ADD CONSTRAINT fk_teacher_id FOREIGN KEY (teacher_id) REFERENCES
	teacher(t_id);
	INSERT INTO teacher(t_name) VALUES('LS1');
	INSERT INTO teacher(t_name) VALUES('LS2');
	INSERT INTO class(c_name, teacher_id) VALUES('bj_a', 1);
	INSERT INTO class(c_name, teacher_id) VALUES('bj_b', 2);

实体类

	public class Teacher {
		private int id;
		private String name;
	}
	public class Classes {
		private int id;
		private String name;
		private Teacher teacher;
	}

ClassMapper.xml映射文件

	<?xml version="1.0" encoding="UTF-8" ?>
		<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
		<mapper namespace="com.silence.mybatis.inter.ClassesMapper">
		<!-- 方式一： 嵌套结果：使用嵌套结果映射来处理重复的联合结果的子集 封装联表查询的数据(去除重复的数据) select * from class 
			c, teacher t where c.teacher_id=t.t_id and c.c_id=1 -->
		<select id="getClass1" parameterType="int" resultMap="ClassResultMap">
			select *
			from class c, teacher t where c.teacher_id=t.t_id and c.c_id=#{id}
		</select>
		<resultMap type="Classes" id="ClassResultMap">
			<id property="id" column="c_id" />
			<result property="name" column="c_name" />
			<association property="teacher" column="teacher_id"
				javaType="Teacher">
				<id property="id" column="t_id" />
				<result property="name" column="t_name" />
			</association>
		</resultMap>
		<!-- 方式二： 嵌套查询：通过执行另外一个 SQL 映射语句来返回预期的复杂类型 SELECT * FROM class WHERE c_id=1; 
			SELECT * FROM teacher WHERE t_id=1 //1 是上一个查询得到的 teacher_id 的值 -->
		<select id="getClass2" parameterType="int" resultMap="ClassResultMap2">
			select *
			from class where c_id=#{id}
		</select>
		<resultMap type="Classes" id="ClassResultMap2">
			<id property="id" column="c_id" />
			<result property="name" column="c_name" />
			<association property="teacher" column="teacher_id"
				javaType="Teacher" select="getTeacher">
			</association>
		</resultMap>
		<select id="getTeacher" parameterType="int" resultType="Teacher">
			SELECT
			t_id id, t_name name FROM teacher WHERE t_id=#{id}
		</select>
	</mapper>
写完上面的映射文件不要忘了在mybatis的核心配置文件加上该配置文件<br>

	<mapper resource="com/silence/mybatis/mapper/ClassMapper.xml" />

接口类
	package com.silence.mybatis.inter;	
	import com.silence.mybatis.enties.Classes;
	
	public interface ClassesMapper {	
		Classes getClass1(int id);		
		Classes getClass2(int id);
	}

测试类

	package com.silence.mybatis;	
	import java.io.IOException;
	import java.io.Reader;
	import org.apache.ibatis.io.Resources;
	import org.apache.ibatis.session.SqlSession;
	import org.apache.ibatis.session.SqlSessionFactory;
	import org.apache.ibatis.session.SqlSessionFactoryBuilder;
	import org.junit.After;
	import org.junit.Before;
	import org.junit.Test;
	
	import com.silence.mybatis.enties.Classes;
	import com.silence.mybatis.inter.ClassesMapper;
	
	public class ClassesTest {
		private SqlSessionFactory sqlMapper = null;
		private SqlSession session = null;
		/*
		 * 一对一关联查询
		 */
		@Test
		public void getClass2(){
			ClassesMapper mapper = session.getMapper(ClassesMapper.class);
			Classes class1 = mapper.getClass2(2);
			System.out.println(class1);
		}
		/*
		 * 一对一关联查询
		 */
		@Test
		public void getClass1(){
			ClassesMapper mapper = session.getMapper(ClassesMapper.class);
			Classes class1 = mapper.getClass1(1);
			System.out.println(class1);
		}
	
		@Before
		public void init() {
			String resource = "MyBatisConfig.xml";
			Reader reader = null;
			try {
				reader = Resources.getResourceAsReader(resource);
			} catch (IOException e) {
				e.printStackTrace();
			}
			sqlMapper = new SqlSessionFactoryBuilder().build(reader);
			session = sqlMapper.openSession();
			session.commit(false);
		}
	
		@After
		public void destory() {
			session.commit();
			session.close();
		}
	
	}
	

##一对多关联查询
根据 classId 查询对应的班级信息,包括学生,老师<br>
数据库脚本

	CREATE TABLE student(
	s_id INT PRIMARY KEY AUTO_INCREMENT,
	s_name VARCHAR(20),
	class_id INT
	);
	INSERT INTO student(s_name, class_id) VALUES('xs_A', 1);
	INSERT INTO student(s_name, class_id) VALUES('xs_B', 1);
	INSERT INTO student(s_name, class_id) VALUES('xs_C', 1);
	INSERT INTO student(s_name, class_id) VALUES('xs_D', 2);
	INSERT INTO student(s_name, class_id) VALUES('xs_E', 2);
	INSERT INTO student(s_name, class_id) VALUES('xs_F', 2);

实体类设计

	public class Student {
		private int id;
		private String name;
	}
	public class Classes {
		private int id;
		private String name;
		private Teacher teacher;
		private List<Student> students;
	}

映射文件 ClassMapper.xml

	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
	<mapper namespace="com.silence.mybatis.inter.ClassesMapper">	
		<!-- 一对多关联查询 -->
		<!-- 方式一: 嵌套结果: 使用嵌套结果映射来处理重复的联合结果的子集 SELECT * FROM class c, teacher t,student 
			s WHERE c.teacher_id=t.t_id AND c.C_id=s.class_id AND c.c_id=1 -->
		<resultMap type="Classes" id="ClassResultMap3">
			<id property="id" column="c_id" />
			<result property="name" column="c_name" />
			<association property="teacher" column="teacher_id"
				javaType="Teacher">
				<id property="id" column="t_id" />
				<result property="name" column="t_name" />
			</association>
			<!-- ofType 指定 students 集合中的对象类型 -->
			<collection property="students" ofType="Student">
				<id property="id" column="s_id" />
				<result property="name" column="s_name" />
			</collection>
		</resultMap>
		<select id="getClass3" parameterType="int" resultMap="ClassResultMap3">
			select *
			from class c, teacher t,student s where c.teacher_id=t.t_id and
			c.C_id=s.class_id and c.c_id=#{id}
		</select>
	
		<!-- 方式二：嵌套查询：通过执行另外一个 SQL 映射语句来返回预期的复杂类型
			 SELECT * FROM class WHERE c_id=1; 
			 SELECT * FROM teacher WHERE t_id=1 //1 是上一个查询得到的 teacher_id 的值 SELECT * FROM 
			 student WHERE class_id=1 //1 是第一个查询得到的 c_id 字段的值 -->
		<select id="getClass4" parameterType="int" resultMap="ClassResultMap4">
			select * from class where c_id=#{id}
		</select>
		<resultMap type="Classes" id="ClassResultMap4">
			<id property="id" column="c_id" />
			<result property="name" column="c_name" />
			<association property="teacher" column="teacher_id"
				javaType="Teacher" select="getTeacher2"></association>
			<collection property="students" ofType="Student" column="c_id"
				select="getStudent"></collection>
		</resultMap>
		<select id="getTeacher2" parameterType="int" resultType="Teacher">
			SELECT t_id id, t_name name FROM teacher WHERE t_id=#{id}
		</select>
		<select id="getStudent" parameterType="int" resultType="Student">
			SELECT s_id id, s_name name FROM student WHERE class_id=#{id}
		</select>
	</mapper>

写完上面的映射文件不要忘了在mybatis的核心配置文件加上该配置文件<br>

	<mapper resource="com/silence/mybatis/mapper/ClassMapper.xml" />

接口类
	package com.silence.mybatis.inter;	
	import com.silence.mybatis.enties.Classes;
	
	public interface ClassesMapper {	
		Classes getClass3(int id);		
		Classes getClass4(int id);
	}

测试类

	package com.silence.mybatis;	
	import java.io.IOException;
	import java.io.Reader;
	import org.apache.ibatis.io.Resources;
	import org.apache.ibatis.session.SqlSession;
	import org.apache.ibatis.session.SqlSessionFactory;
	import org.apache.ibatis.session.SqlSessionFactoryBuilder;
	import org.junit.After;
	import org.junit.Before;
	import org.junit.Test;
	
	import com.silence.mybatis.enties.Classes;
	import com.silence.mybatis.inter.ClassesMapper;
	
	public class ClassesTest {
		private SqlSessionFactory sqlMapper = null;
		private SqlSession session = null;
		/*
		 * 集合查询，查询该班级下的所有教师学生的信息，一对多关联查询
		 */
		@Test
		public void getClass4(){
			ClassesMapper mapper = session.getMapper(ClassesMapper.class);
			Classes class1 = mapper.getClass4(2);
			System.out.println(class1);
		}
		/*
		 * 集合查询，查询该班级下的所有教师学生的信息，一对多关联查询
		 */
		@Test
		public void getClass3(){
			ClassesMapper mapper = session.getMapper(ClassesMapper.class);
			Classes class1 = mapper.getClass3(2);
			System.out.println(class1);
		}
		@Before
		public void init() {
			String resource = "MyBatisConfig.xml";
			Reader reader = null;
			try {
				reader = Resources.getResourceAsReader(resource);
			} catch (IOException e) {
				e.printStackTrace();
			}
			sqlMapper = new SqlSessionFactoryBuilder().build(reader);
			session = sqlMapper.openSession();
			session.commit(false);
		}
		@After
		public void destory() {
			session.commit();
			session.close();
		}
	}

##动态 SQL与模糊查询
准备数据库脚本
	
	create table d_user(
	id int primary key auto_increment,
	name varchar(10),
	age int(3)
	);
	insert into d_user(name,age) values('Tom',12);
	insert into d_user(name,age) values('Bob',13);
	insert into d_user(name,age) values('Jack',18);

条件类的设计
	
	public class ConditionUser {
		private String name;
		private int minAge;
		private int maxAge;
	}

Customer类设计
	
	public class Customer{
		private int id;
		private String name;
		private int age;
	}

CustomerMapper.xml映射文件
	
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
	"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
	<mapper namespace="com.silence.mybatis.inter.CustomerMapper">
		<select id="getCustomer" parameterType="com.silence.mybatis.enties.ConditionUser"
			resultType="com.silence.mybatis.enties.Customer">
			select * from d_user where age>=#{minAge} and age&lt;=#{maxAge}
			<if test='name!="%null%"'>and name like #{name}</if>
		</select>
	</mapper>

测试
	
	@Test
	public void getCustomer(){
		CustomerMapper mapper = session.getMapper(CustomerMapper.class);
		List<Customer> customer = mapper.getCustomer(new ConditionUser("%a%", 1, 18));
		System.out.println(customer.size());
	}
	
##调用存储过程
查询得到男性或女性的数量,  如果传入的是 0  就女性否则是男性<br>
数据库脚本

	create table p_user(
	id int primary key auto_increment,
	name varchar(10),
	sex char(2)
	);
	insert into p_user(name,sex) values('A',"男");
	insert into p_user(name,sex) values('B',"女");
	insert into p_user(name,sex) values('C',"男");
	创建存储过程
	DELIMITER $
	CREATE PROCEDURE mybatis.ges_user_count(IN sex_id INT, OUT user_count INT)
	BEGIN
	IF sex_id=0 THEN
	SELECT COUNT(*) FROM mybatis.p_user WHERE p_user.sex='女' INTO user_count;
	ELSE
	SELECT COUNT(*) FROM mybatis.p_user WHERE p_user.sex='男' INTO user_count;
	END IF;
	END
	$
	调用存储过程
	DELIMITER ;
	SET @user_count = 0;
	CALL mybatis.ges_user_count(1, @user_count);
	SELECT @user_count;

创建实体对象

	public class PUser {
		private String id;
		private String name;
		private String sex;
	}

创建PUserMapper.xml

	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
	<mapper namespace="com.silence.mybatis.enties.PUserMapper">
		<select id="getCount" resultType="java.util.Map" statementType="CALLABLE">
			{call
			ges_user_count(#{sex_id,mode=IN,jdbcType=INTEGER},#{result,mode=OUT,jdbcType=INTEGER})
			}
		</select>
	</mapper>

定义接口

	package com.silence.mybatis.inter;
	import java.util.Map;
	
	public interface PUserMapper {
	
		public Object getCount(Map<String, Integer> map);
	}

存储过程的掉调用

	/*
	 * 存储过程的调用
	 */
	@Test
	public void getCount(){
		PUserMapper mapper = session.getMapper(PUserMapper.class);
		Map<String, Integer> map = new HashMap<>();
		map.put("sex_id", 1);
		mapper.getCount(map);
		System.out.println(map.get("result"));
	}

##缓存的使用
许多应用程序，为了提高性能而增加缓存, 特别是从数据库中获取的数据. 在默认情况下，Mybatis 的一级缓存是默认开启的。类似于 Hibernate, 所谓一级缓存，也就是基于同一个 sqlsession 的查询语句，即 session 级别的缓存，非全局缓存，或者非二级缓存.<br><br>
1. 一级缓存: 基于 PerpetualCache 的 HashMap 本地缓存，其存储作用域为 Session，当 Session flush或close 之后，该 Session 中的所有 Cache 就将清空。<br>
2. 二级缓存与一级缓存其机制相同，默认也是采用 PerpetualCache，HashMap 存储，不同在于
其存储作用域为 Mapper(Namespace)，并且 可自定义存储源，如 Ehcache。<br>
3.  对于缓存数据更新机制，当某一个作用域(一级缓存 Session/二级缓存 Namespaces)的进行了
C/U/D 操作后，默认该作用域下所有 select 中的缓存将被 clear。<br><br>
如果要实现 Mybatis 的二级缓存，一般来说有如下两种方式:<br>
1. 采用 Mybatis 内置的 cache 机制。<br>
2. 采用三方 cache 框架， 比如 ehcache, oscache 等等。<br>
采用 Mybatis 内置的 cache 机制<br>
在SQL语句映射文件中加入 语句 , 并且相应的 model 类要实现 java Serializable 接口，因为缓存说白了就是序列化与反序列化的过程，所以需要实现这个接口. 单纯的 表示如下意思:<br>
1. 所有在映射文件里的 select 语句都将被缓存。<br>
2. 所有在映射文件里 insert,update 和 delete 语句会清空缓存。<br>
3. 缓存使用“最近很少使用”算法来回收<br>
4. 缓存不会被设定的时间所清空。<br>
5. 每个缓存可以存储 1024 个列表或对象的引用（不管查询出来的结果是什么） 。<br>
6. 缓存将作为“读/写”缓存，意味着获取的对象不是共享的且对调用者是安全的。不会有其它的调用者或线程潜在修改。<br><br>
7. 
缓存元素的所有特性都可以通过属性来修改。比如：<br>
#
	<cache eviction="FIFO" flushInterval="60000" size="512" readOnly="true" />
#
采用 ehcache 来实现 Mybatis 的二级缓存<br>
首先需要在 Mybatis 的官网上下载相关jar 包：https://code.google.com/p/mybatis/ 写文档的时候下载的是：mybatis-ehcache-1.0.2.zip ，里面包括了<br>
mybatis-ehcache-1.0.2.jar<br>
ehcache-core-2.6.5.jar<br>
slf4j-api-1.6.1.jar<br>
当然，采用 ehcache 就必须在 classpath 下 加入 ehcache 的配置文件 ehcache.xml:<br>
#	
	<cache name="default"
	maxElementsInMemory="10000"
	eternal="false"
	timeToIdleSeconds="3600"
	timeToLiveSeconds="10"
	overflowToDisk="true"
	diskPersistent="true"
	diskExpiryThreadIntervalSeconds="120"
	maxElementsOnDisk="10000"
	/>
#
那么在 SQL 映射文件中要如何配置呢，参考如下：<br>
#
	<cache type="org.mybatis.caches.ehcache.LoggingEhcache" >
	<property name="timeToIdleSeconds" value="3600"/><!--1 hour-->
	<property name="timeToLiveSeconds" value="3600"/><!--1 hour-->
	<property name="maxEntriesLocalHeap" value="1000"/>
	<property name="maxEntriesLocalDisk" value="10000000"/>
	<property name="memoryStoreEvictionPolicy" value="LRU"/>
	</cache>
#
总结：无论是采用 Mybatis 自身的 cache 还是三方的 cache , 这样的配置，就是对所有的 select 语句都全局缓存，但事实上，并不总是这样，比如，我在这系列教程实现 Mybatis 分页 () ，自己写的分页算法，就不能用这种情况。需要禁止掉 cache所以需要如下方法:
#
	<select id="selectArticleListPage" resultMap="resultUserArticleList" useCache="false">
	.......
#
注意到 useCache="false" 了吗？ 这可以避免使用缓存。<br>
我们可以在之前的例子中来测试缓存<br>
一级缓存测试，一级缓存是默认的
	/*
	 * 一级缓存的测试
	 */
	@Test
	public void testCache1(){
		IUser iu = session.getMapper(IUser.class);
		System.out.println(iu.findById(1));
		System.out.println("-----------------------");
		System.out.println(iu.findById(1));
		System.out.println(sqlMapper.openSession().getMapper(IUser.class).findById(1));
		//执行了缓存的清除后或重新发送sql到数据库中重新查询一次
		session.clearCache();
		System.out.println(iu.findById(1));
	}

使用二级缓存<br>
(1)首先需要在映射文件中加入<cache></cache><br>
(2)需要使用二级缓存的类必须实现Serializable接口，不然会报java.io.NotSerializableException错误
(3)然后我们就可以测试了
	
	/*
	 * 二级缓存的测试
	 */
	@Test
	public void testCache2(){
		IUser iu = session.getMapper(IUser.class);
		System.out.println(iu.findById(1));
		session.commit();
		session.close();
		session = sqlMapper.openSession();
		iu = session.getMapper(IUser.class);
		System.out.println(iu.findById(1));
	}
