package com.silence.mybatis.enties;

public class Category {
    private Integer id;

    private String catname;

    private String catdescription;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname == null ? null : catname.trim();
    }

    public String getCatdescription() {
        return catdescription;
    }

    public void setCatdescription(String catdescription) {
        this.catdescription = catdescription == null ? null : catdescription.trim();
    }

	public Category(Integer id, String catname, String catdescription) {
		super();
		this.id = id;
		this.catname = catname;
		this.catdescription = catdescription;
	}

	public Category() {
		super();
	}

	public Category(String catname, String catdescription) {
		super();
		this.catname = catname;
		this.catdescription = catdescription;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", catname=" + catname + ", catdescription=" + catdescription + "]";
	}
 
}