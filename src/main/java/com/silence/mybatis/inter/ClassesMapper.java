package com.silence.mybatis.inter;

import com.silence.mybatis.enties.Classes;

public interface ClassesMapper {

	Classes getClass1(int id);
	
	Classes getClass2(int id);
	
	Classes getClass3(int id);
	
	Classes getClass4(int id);
}
