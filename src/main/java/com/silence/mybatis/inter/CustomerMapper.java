package com.silence.mybatis.inter;

import java.util.List;

import com.silence.mybatis.enties.ConditionUser;
import com.silence.mybatis.enties.Customer;

public interface CustomerMapper {

	List<Customer> getCustomer(ConditionUser con);
}
