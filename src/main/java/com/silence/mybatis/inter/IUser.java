package com.silence.mybatis.inter;

import java.util.List;
import java.util.Map;

import com.silence.mybatis.enties.Article;
import com.silence.mybatis.enties.User;

public interface IUser {
	/*
	 * 这里面有一个方法名 findById 必须与 User.xml 里面配置的 select 的id 对应
	 * 确保namespace属性值和接口的全限定名相同，且id属性值和接口方法名相同：
	 */
	public User findById(int id);

	public List<User> selectUsers(String userName);

	public void addUser(User user);

	public void updateUser(User user);

	public void deleteUser(int id);

	public List<Article> getUserArticles(int uid);

	public List<Article> getUserArticlesPage(Map<Object, Object> map);

	public List<User> testIf(User user);

	public List<User> testChoose(User user);

	public List<User> testTrim(User user);

	public List<User> testWhere(User user);

	public void testUpdate(User user);

	public List<User> testForeach(List<Integer> ids);

	public List<User> testForeachArray(int[] array);

	public List<User> testForeachMap(Map<Object, Object> map);
}
