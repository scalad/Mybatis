package com.silence.mybatis.inter;

import com.silence.mybatis.enties.Order;

public interface OrderMapper {
   
	Order selectOrder(int id);
	
	Order selectOrderResultMap(int id);
}