package com.silence.mybatis;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.silence.mybatis.enties.Category;
import com.silence.mybatis.inter.CategoryMapper;

public class CategoryTest {
	private SqlSessionFactory sqlMapper = null;
	private SqlSession session = null;

	
	@Test
	public void testSelectAllCategoryByAnnocation(){
		CategoryMapper mapper = session.getMapper(CategoryMapper.class);
		List<Category> categorys = mapper.getAllCategorys();
		for (Category g : categorys){
			System.out.println(g);
		}
	}
	@Test
	public void testinsert() {
		CategoryMapper mapper = session.getMapper(CategoryMapper.class);
		mapper.insert(new Category("test", "canyou"));
	}

	@Test
	public void testselectByPrimaryKey() {
		CategoryMapper mapper = session.getMapper(CategoryMapper.class);
		Category category = mapper.selectByPrimaryKey(8);
		System.out.println(category);
	}

	@Before
	public void init() {
		String resource = "MyBatisConfig.xml";
		Reader reader = null;
		try {
			reader = Resources.getResourceAsReader(resource);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		session = sqlMapper.openSession();
		session.commit(false);
	}

	@After
	public void destory() {
		session.commit();
		session.close();
	}

}
