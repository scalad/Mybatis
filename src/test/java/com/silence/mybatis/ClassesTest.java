package com.silence.mybatis;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.silence.mybatis.enties.Classes;
import com.silence.mybatis.inter.ClassesMapper;

public class ClassesTest {
	private SqlSessionFactory sqlMapper = null;
	private SqlSession session = null;

	/*
	 * 集合查询，查询该班级下的所有教师学生的信息，一对多关联查询
	 */
	@Test
	public void getClass4(){
		ClassesMapper mapper = session.getMapper(ClassesMapper.class);
		Classes class1 = mapper.getClass4(2);
		System.out.println(class1);
	}

	/*
	 * 集合查询，查询该班级下的所有教师学生的信息，一对多关联查询
	 */
	@Test
	public void getClass3(){
		ClassesMapper mapper = session.getMapper(ClassesMapper.class);
		Classes class1 = mapper.getClass3(2);
		System.out.println(class1);
	}
	/*
	 * 一对一关联查询
	 */
	@Test
	public void getClass2(){
		ClassesMapper mapper = session.getMapper(ClassesMapper.class);
		Classes class1 = mapper.getClass2(2);
		System.out.println(class1);
	}
	/*
	 * 一对一关联查询
	 */
	@Test
	public void getClass1(){
		ClassesMapper mapper = session.getMapper(ClassesMapper.class);
		Classes class1 = mapper.getClass1(1);
		System.out.println(class1);
	}

	@Before
	public void init() {
		String resource = "MyBatisConfig.xml";
		Reader reader = null;
		try {
			reader = Resources.getResourceAsReader(resource);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		session = sqlMapper.openSession();
		session.commit(false);
	}

	@After
	public void destory() {
		session.commit();
		session.close();
	}

}
