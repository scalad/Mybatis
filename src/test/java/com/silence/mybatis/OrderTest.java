package com.silence.mybatis;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.silence.mybatis.enties.ConditionUser;
import com.silence.mybatis.enties.Customer;
import com.silence.mybatis.enties.Order;
import com.silence.mybatis.inter.CustomerMapper;
import com.silence.mybatis.inter.OrderMapper;

public class OrderTest {
	private SqlSessionFactory sqlMapper = null;
	private SqlSession session = null;
	
	@Test
	public void getCustomer(){
		CustomerMapper mapper = session.getMapper(CustomerMapper.class);
		List<Customer> customer = mapper.getCustomer(new ConditionUser("%a%", 1, 18));
		System.out.println(customer.size());
	}
	
	@Test
	public void selectOrderResultMap(){
		OrderMapper mapper = session.getMapper(OrderMapper.class);
		Order order = mapper.selectOrderResultMap(2);
		System.out.println(order);
	}
	
	@Test
	public void selectOrder() {
		OrderMapper mapper = session.getMapper(OrderMapper.class);
		Order order = mapper.selectOrder(1);
		System.out.println(order);
	}

	@Before
	public void init() {
		String resource = "MyBatisConfig.xml";
		Reader reader = null;
		try {
			reader = Resources.getResourceAsReader(resource);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		session = sqlMapper.openSession();
		session.commit(false);
	}

	@After
	public void destory() {
		session.commit();
		session.close();
	}

}
