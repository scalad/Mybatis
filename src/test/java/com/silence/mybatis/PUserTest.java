package com.silence.mybatis;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.silence.mybatis.inter.PUserMapper;

public class PUserTest {
	private SqlSessionFactory sqlMapper = null;
	private SqlSession session = null;

	/*
	 * 存储过程的调用
	 */
	@Test
	public void getCount(){
		PUserMapper mapper = session.getMapper(PUserMapper.class);
		Map<String, Integer> map = new HashMap<>();
		map.put("sex_id", 1);
		mapper.getCount(map);
		System.out.println(map.get("result"));
		System.out.println(map.get("sex_id"));
	}
	
	@Before
	public void init() {
		String resource = "MyBatisConfig.xml";
		Reader reader = null;
		try {
			reader = Resources.getResourceAsReader(resource);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		session = sqlMapper.openSession();
		session.commit(false);
	}

	@After
	public void destory() {
		session.commit();
		session.close();
	}

}
