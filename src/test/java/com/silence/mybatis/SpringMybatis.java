package com.silence.mybatis;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.silence.mybatis.enties.Article;
import com.silence.mybatis.enties.User;
import com.silence.mybatis.inter.IUser;

public class SpringMybatis {

	private static ApplicationContext ctx;

	static {
		ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
	}
	
	//@Test
	public void test(){
		IUser mapper = ctx.getBean("userMapper", IUser.class);
		//测试id=1的用户查询，根据数据库中的情况，可以改成你自己的.
		System.out.println("得到用户id=1的用户信息");
		User user = mapper.findById(2);
		System.out.println(user);
		
		List<Article> articles = mapper.getUserArticles(1);
		for (Article a : articles){
			System.out.println(a);
		}
	}
}
