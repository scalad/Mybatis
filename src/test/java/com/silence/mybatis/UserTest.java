package com.silence.mybatis;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.silence.mybatis.enties.Article;
import com.silence.mybatis.enties.User;
import com.silence.mybatis.inter.IUser;

public class UserTest {

	private SqlSessionFactory sqlMapper = null;
	private SqlSession session = null;

	@Before
	public void init() {
		String resource = "MyBatisConfig.xml";
		Reader reader = null;
		try {
			reader = Resources.getResourceAsReader(resource);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		session = sqlMapper.openSession();
		session.commit(false);
	}

	@After
	public void destory() {
		session.commit();
		session.close();
	}
	/*
	 * 二级缓存的测试
	 */
	@Test
	public void testCache2(){
		IUser iu = session.getMapper(IUser.class);
		System.out.println(iu.findById(1));
		session.commit();
		session.close();
		session = sqlMapper.openSession();
		iu = session.getMapper(IUser.class);
		System.out.println(iu.findById(1));
	}
	
	/*
	 * 一级缓存的测试
	 */
	@Test
	public void testCache1(){
		IUser iu = session.getMapper(IUser.class);
		System.out.println(iu.findById(1));
		System.out.println("-----------------------");
		System.out.println(iu.findById(1));
		System.out.println(sqlMapper.openSession().getMapper(IUser.class).findById(1));
		session.clearCache();
		System.out.println(iu.findById(1));
	}
	
	/*
	 * foreach测试，map数据
	 */
	@Test
	public void testForeachMap() {
		IUser iu = session.getMapper(IUser.class);
		Map<Object, Object> map = new HashMap<>();
		map.put("userName", "test");
		map.put("ids", Arrays.asList(1, 2, 3, 4, 5, 6, 7, 9, 10, 11));
		List<User> users = iu.testForeachMap(map);
		printUsers(users);
	}

	/*
	 * foreach测试,array类型参数
	 */
	@Test
	public void testForeachArray() {
		IUser iu = session.getMapper(IUser.class);
		List<User> users = iu.testForeachArray(new int[] { 1, 2, 3 });
		printUsers(users);
	}

	/*
	 * foreach测试,list类型参数
	 */
	@Test
	public void testForeach() {
		IUser iu = session.getMapper(IUser.class);
		List<User> users = iu.testForeach(Arrays.asList(1, 2, 3, 4));
		printUsers(users);
	}

	/*
	 * 更新测试
	 */
	@Test
	public void testUpdate() {
		IUser iu = session.getMapper(IUser.class);
		iu.testUpdate(new User(1, "github"));
	}

	/*
	 * where测试代码
	 */
	@Test
	public void testWhere() {
		IUser iu = session.getMapper(IUser.class);
		List<User> users = iu.testWhere(new User("silence", "silence"));
		printUsers(users);
	}

	/*
	 * tirm测试代码
	 */
	@Test
	public void testTrim() {
		IUser iu = session.getMapper(IUser.class);
		List<User> users = iu.testTrim(new User("silence", "123456"));
		printUsers(users);
	}

	/*
	 * choose测试代码
	 */
	@Test
	public void testChoose() {
		IUser iu = session.getMapper(IUser.class);
		List<User> list = iu.testChoose(new User("silence", "123456"));
		printUsers(list);
	}

	/*
	 * if测试代码
	 */
	@Test
	public void testif() {
		IUser iu = session.getMapper(IUser.class);
		List<User> list = iu.testIf(new User("silence", null));
		printUsers(list);
	}

	/*
	 * 分页查询
	 */
	@Test
	public void getUserArticlesPage() {
		IUser iu = session.getMapper(IUser.class);
		Map<Object, Object> map = new HashMap<>();
		map.put("id", 1);
		map.put("offset", 2);
		map.put("pagesize", 2);
		List<Article> articles = iu.getUserArticlesPage(map);
		printUsers(articles);
	}

	/*
	 * 一对多查询
	 */
	@Test
	public void getUserArticles() {
		IUser iu = session.getMapper(IUser.class);
		List<Article> articles = iu.getUserArticles(1);
		printUsers(articles);
	}

	/*
	 * 更新数据
	 */
	@Test
	public void deleteUser() {
		IUser iu = session.getMapper(IUser.class);
		iu.deleteUser(8);
	}

	/*
	 * 更新数据
	 */
	@Test
	public void updateUser() {
		IUser iu = session.getMapper(IUser.class);
		iu.updateUser(new User(7, "王大帅", "65432121"));
	}

	/*
	 * 插入数据
	 */
	@Test
	public void addUser() {
		IUser iu = session.getMapper(IUser.class);
		iu.addUser(new User("王培坤", "123456"));
	}

	/*
	 * 查询集合对象
	 */
	@Test
	public void getUserList() {
		IUser iu = session.getMapper(IUser.class);
		List<User> users = iu.selectUsers("%t");
		printUsers(users);
	}

	/*
	 * 面向接口编程测试
	 */
	@Test
	public void iUserTest() {
		IUser u = session.getMapper(IUser.class);
		User user = u.findById(2);
		System.out.println(user);
	}

	@Test
	public void userTest() {
		User user = session.selectOne("findById", 2);
		System.out.println(user);
	}

	public static <T> void printUsers(List<T> l) {
		for (T t : l) {
			System.out.println(t);
		}
	}
}
